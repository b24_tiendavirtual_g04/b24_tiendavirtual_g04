
import executeQuery
 from "../services/mysql.service";
const obtenerCamisas = async (req, res) => {
    //res.send('Obtener camisas desde el controlador')
    try{
        const response = await executeQuery('SELECT * FROM inventario');
        const data = {
            message: `${response.length} datos encontrados`,
            datos: response.length > 0 ? response : null
        }
        res.json(data);
    }catch(error){
        console.log(error);
        res.status(500).send(error);
    }
}

const obtenerCamisa = (req, res) => {
    //res.send('Obtener camisas desde el controlador')
    const {id} = req.params;
    executeQuery(`SELECT * FROM inventario WHERE idinventario = ${id}`).then((response) => {
        const data = {
            message: `${response.length} datos encontrados`,
            datos: response.length > 0 ? response[0] : null
        }
        res.json(data);
    }).catch((error) => {
        console.log(error);
        res.status(500).send(error);
    });
}

const agregarCamisa = async (req, res, next) => {
    //res.send('Agregar camisas desde el controlador')
    const {idinventario, imagen, nombre, talla, material, color, cantidad, marca, precio} = req.body;
    try{
        const response = await executeQuery(`INSERT INTO inventario (idinventario, imagen, nombre, talla, material, color, cantidad, marca, precio) VALUES ('${idinventario}','${imagen}', '${nombre}', '${talla}','${material}','${color}','${cantidad}','${marca}','${precio}')`);
        res.status(201).json({message: 'created', id: response.insertId});
    }catch(error){
        console.log(error);
        res.status(500).send(error);
    }
}

const actualizarCamisa = async (req, res) => {
    //res.send('Actualizar camisas desde el controlador')
    const {imagen, nombre, talla, material, color, cantidad, marca, precio} = req.body;
    try{
        const response = await executeQuery(`UPDATE iventario SET imagen = '${imagen},'nombre = '${nombre}', talla = '${talla}', material = '${material}', color = '${color}', cantidad = '${cantidad}', marca = '${marca}', precio = '${precio}' WHERE idinventario = ${req.params.id}`);
        console.log(response);
        if(response.affectedRows > 0){
            res.json({message: 'updated'});
        }else{
            res.status(404).json({message: `No existe registro con id: ${req.params.id}`})
        }
    }catch(error){
        console.log(error);
        res.status(500).send(error);
    }
}

const eliminarCamisa = async (req, res) => {
    //res.send('Eliminar camisas desde el controlador')
    try{
        const response = await executeQuery(`DELETE FROM inventario WHERE idinventario = ${req.params.id}`);
        console.log(response);
        if(response.affectedRows > 0){
            res.json({message: 'deleted'});
        }else{
            res.status(404).json({message: `No existe registro con id: ${req.params.id}`})
        }
    }catch(error){
        console.log(error);
        res.status(500).send(error);
    }
}

export {obtenerCamisas, obtenerCamisa, agregarCamisa, actualizarCamisa, eliminarCamisa};