import {Router} from "express"
import {obtenerMedias} from "../controllers/mediaController"

const mediaRoutes = (app) => {

    const router = Router();
    app.use('/', router);

    router.get('/obtenerMedias', obtenerMedias);
    router.get('/obtenerMedia/:id', (req, res) => res.send('Obtener media'));
    router.post('/agregarMedia', (req, res) => res.send('Agregar medias'));
    router.put('/actualizarMedia/:id', (req, res) => res.send('Actualizar medias'));
    router.delete('/eliminarMedia/:id', (req, res) => res.send('Eliminar medias'));
}

export default mediaRoutes;