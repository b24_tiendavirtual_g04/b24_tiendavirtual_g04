import {Router} from "express"
import {obtenerPantalones} from "../controllers/pantalonController"

const pantalonRoutes = (app) => {

    const router = Router();
    app.use('/', router);

    router.get('/obtenerPantalones', obtenerPantalones);
    router.get('/obtenerPantalon/:id', (req, res) => res.send('Obtener pantalon'));
    router.post('/agregarPantalon', (req, res) => res.send('Agregar pantalones'));
    router.put('/actualizarPantalon/:id', (req, res) => res.send('Actualizar pantalones'));
    router.delete('/eliminarPantalon/:id', (req, res) => res.send('Eliminar pantalones'));
}

export default pantalonRoutes;