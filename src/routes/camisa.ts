import {Router} from "express"
import { obtenerCamisas } from "../controllers/camisaController";

const camisaRoutes = (app) => {

    const router = Router();

    app.use('/', router);

    router.get('/obtenerCamisas', obtenerCamisas);
    router.get('/obtenerCamisa/:id', (req, res) => res.send('Obtener camisa'));
    router.post('/agregarCamisa', (req, res) => res.send('Agregar camisas'));
    router.put('/actualizarCamisa/:id', (req, res) => res.send('Actualizar camisas'));
    router.delete('/eliminarCamisa/:id', (req, res) => res.send('Eliminar camisas'));

}

export default camisaRoutes;