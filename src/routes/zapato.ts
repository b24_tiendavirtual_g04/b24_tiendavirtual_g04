import {Router} from "express"
import {obtenerZapatos_sport} from "../controllers/zapatoController"

const zapato_sportRoutes = (app) => {

    const router = Router();
    app.use('/', router);

    router.get('/obtenerZapatos_sport', obtenerZapatos_sport);
    router.get('/obtenerZapato_sport/:id', (req, res) => res.send('Obtener zapato_sport'));
    router.post('/agregarZapato_sport', (req, res) => res.send('Agregar zapatos_sport'));
    router.put('/actualizarZapato_sport/:id', (req, res) => res.send('Actualizar zapatos_sport'));
    router.delete('/eliminarZapato_sport/:id', (req, res) => res.send('Eliminar zapatos_sport'));
}

export default zapato_sportRoutes;