import express from 'express';
import camisaRoutes from './routes/camisa';
import config from './config/config';
import zapato_sportRoutes from './routes/zapato';

const app = express();
//const port = 3000;

app.use(express.json());
app.use(express.urlencoded({ extended: true}));

camisaRoutes(app);
zapato_sportRoutes(app);

app.get('/', async(req,res, next) => {
    //console.log('Antes de la promesa');

    const datos = {
        imagen: 'camisa.jpg',
        nombre: 'camisa',
        talla: 'XL',
        material: 'algodon',
        color: 'azul',
        cantidad: '100',
        marca: 'pat primo',
        precio: '30000'
    }
    
    // desestructuración
    const {imagen} = datos; 
    res.status(200).json({datos: imagen});

    //let x = 10;
    //const promesa = new Promise((resolve, reject) => {
    //    if (x == 10){
    //        resolve('promesa resuelta');
    //    } else {
    //        reject('promesa rechazada');
    //    }
    //});
    //console.log(req.headers);
    //console.log(req.params);
    //console.log(req.query);
    //xconsole.log(req.body);

    //promesa.then((res) =>{
    //    console.log(res);
    //}).catch((error)=>{
    //    console.log(error)
    //});
    //console.log('Después de la promesa')
    res.send('prueba del servidor');
    
});

app.listen(config.PORT, () =>{
    return console.log(`servidor corriendo sobre el puerto ${config.PORT}`)
});

console.log("Prueba")